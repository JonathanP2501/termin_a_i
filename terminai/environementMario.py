import numpy as np
import random
import os
import csv
import sys
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPen, QColor, QBrush
from typing import *
from geneticUtil import Individual, Population
from neuralNetwork import linear, sigmoid, tanh, relu, leaky_relu, NeuralNetwork, Activate, \
    get_activation as get_activation
from utils import SuperMarioBros, StaticTileRAMmap, DynamicTileRAMmap, EnemyRAMmap

hidden_layer_tab=[9]
t_width=7
t_height=10

class EnvironementMario(Individual):
    def __init__(self, gene: Optional[Dict[str, np.ndarray]] = None, hidden_activ: Optional[Activate] = 'relu',
                 output_activ: Optional[Activate] = 'sigmoid', hidden_layer_arch: List[int] = [12, 9],
                 lifespan: Union[int, float] = np.inf, name: Optional[str] = None, debug: Optional[bool] = False):


        self.lifespan = lifespan

        self.name = name
        self.debug = debug
        self.frames_ = 0
        self.fitness_ = 0
        self.frames_after_locked = 0
        self.row_strt = 4 #where the tiles are starting

        self.hidden_layer_architecture = hidden_layer_tab
        self.hidden_layer_actv = 'relu'
        self.output_act = 'sigmoid'

        self.tiles_width = t_width
        self.tiles_height = t_height


        num_inputs = self.tiles_width * self.tiles_height + self.tiles_height


        self.inputs_as_array = np.zeros((num_inputs, 1))
        self.nn_architecture = [num_inputs]  # Input Nodes
        self.nn_architecture.extend(self.hidden_layer_architecture)  # Hidden Layer Ndoes
        self.nn_architecture.append(6)  # 6 Outputs for each button

        self.network = NeuralNetwork(self.nn_architecture,
                                     get_activation(self.hidden_layer_actv),
                                     get_activation(self.output_act)
                                     )

        if gene:
            self.network.parameters = gene

        self.is_alive = True
        self.x_dist = None
        self.game_score = None
        self.win = False
        # This is mainly just to "see" Mario winning
        self.allow_additional_time = True
        self.additional_timesteps = 0
        self.max_additional_timesteps = int(60 * 2.5)

        self.button = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0], np.int8)
        self.max_dist_mario = 0

    @property
    def fitness(self):
        return self.fitness_

    def gene(self):
        pass

    def gene_decoding(self):
        pass

    def gene_encoding(self):
        pass

    def input_to_array(self, game_ram, game_tiles) -> None:
        mario_row, mario_col = SuperMarioBros.get_col_mario(game_ram)
        temp = []

        for row in range(self.row_strt, self.row_strt + self.tiles_height):
            for col in range(mario_col, mario_col + self.tiles_width):
                try:
                    t = game_tiles[(row, col)]
                    if isinstance(t, StaticTileRAMmap):
                        if t.value == 0:
                            temp.append(0)
                        else:
                            temp.append(1)
                    else:
                        temp.append(-1)
                except:
                    t = StaticTileRAMmap(0x00)
                    temp.append(0)  # Empty

        self.inputs_as_array[:self.tiles_height * self.tiles_width, :] = np.array(temp).reshape((-1, 1))

        row = mario_row - self.row_strt
        tiles_array = np.zeros((self.tiles_height, 1))

        if 0 <= row < self.tiles_height:
            tiles_array[row, 0] = 1
        self.inputs_as_array[self.tiles_height * self.tiles_width:, :] = tiles_array.reshape((-1, 1))

    def update(self, game_ram, game_tiles, output_to_buttons_map) -> bool:
        if self.is_alive:
            self.game_score = SuperMarioBros.get_score(game_ram)
            self.x_dist = SuperMarioBros.get_mario_position(game_ram).x
            self.frames_ += 1

            if game_ram[0x001D] == 3:  # victory in RAMmap
                self.win = True
                if not self.allow_additional_time:
                    self.is_alive = False
                    return False
            if self.x_dist > self.max_dist_mario:
                self.max_dist_mario = self.x_dist
                self.frames_after_locked = 0 #we've moved on reset counter
            else:
                self.frames_after_locked += 1

            if self.win:
                self.additional_timesteps += 1

            if self.additional_timesteps > self.max_additional_timesteps:
                self.is_alive = False
                return False
            elif not self.win and self.frames_after_locked > 60 * 4: #4 seconds delay
                self.is_alive = False
                return False
        else:
            return False

        if game_ram[0x0E] in (0x0B, 0x06) or game_ram[0xB5] == 2: #falling
            self.is_alive = False
            return False

        self.input_to_array(game_ram, game_tiles)

        # Calculate the output
        output = self.network.nn_get_output(self.inputs_as_array)
        marge = np.where(output > 0.5)[0]
        self.button.fill(0)  # Clear

        # Set buttons
        for b in marge:
            self.button[output_to_buttons_map[b]] = 1

        return True

    def get_fitness_mario(self):
        frames = self.frames_
        distance = self.x_dist
        score = self.game_score

        self.fitness_ = fonction_fitness(frames, distance, self.win)
        if(self.fitness_>1):
            print("Current fitness :" + str(self.fitness_))
        else:
            print("Current fitness : 0")

        print("Current progress :" + str(self.max_dist_mario)+"\n")

def fonction_fitness(frames, distanceMax, did_win):
    return max(distanceMax ** 1.8 - frames ** 1.5 + min(max(distanceMax - 50, 0), 1) * 2500 + did_win * 1e4, 0.00001)

def get_stats(stat_dat: List[Union[int, float]]) -> Tuple[float, float, float, float, float]:
    mean = np.mean(stat_dat)
    median = np.median(stat_dat)
    std = np.std(stat_dat)
    _min = float(min(stat_dat))
    _max = float(max(stat_dat))

    return (mean, median, std, _min, _max)


def save(population_path: str, individual_name: str, mario: EnvironementMario) -> None:

    if not os.path.exists(population_path): #after each gen
        os.makedirs(population_path)
    ind_directory = os.path.join(population_path, individual_name)
    os.makedirs(ind_directory)

    layers_list = len(mario.network.layer_nodes)
    for layer in range(1, layers_list):
        weight_str = 'Weight' + str(layer)
        weights = mario.network.parameters[weight_str]

        biais_str = 'Biais' + str(layer)
        bias = mario.network.parameters[biais_str]

        np.save(os.path.join(ind_directory, weight_str), weights)
        np.save(os.path.join(ind_directory, biais_str), bias)


def save_stats(population: Population, fname: str):
    directory = os.path.dirname(fname)
    if not os.path.exists(directory):
        os.makedirs(directory)

    f = fname

    frames = [individual.frames_ for individual in population.individuals]
    max_distance = [individual.max_dist_mario for individual in population.individuals]
    fitness = [individual.fitness for individual in population.individuals]
    wins = [sum([individual.win for individual in population.individuals])]

    write_header = True
    if os.path.exists(f):
        write_header = False

    trackers = [('frames', frames),
                ('distance', max_distance),
                ('fitness', fitness),
                ('wins', wins)
                ]

    stats = ['mean', 'median', 'std', 'min', 'max']

    header = [t[0] + '_' + s for t in trackers for s in stats]

    with open(f, 'a') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=header, delimiter=',')
        if write_header:
            writer.writeheader()

        row = {}
        # Create a row to insert into csv
        for tracker_name, tracker_object in trackers:
            curr_stats = get_stats(tracker_object)
            for curr_stat, stat_name in zip(curr_stats, stats):
                entry_name = '{}_{}'.format(tracker_name, stat_name)
                row[entry_name] = curr_stat

        # Write row
        writer.writerow(row)




def load_stats(path_to_stats: str, normalize: Optional[bool] = False):
    data = {}

    fieldnames = None
    trackers_stats = None
    trackers = None
    stats_names = None

    with open(path_to_stats, 'r') as csvfile:
        reader = csv.DictReader(csvfile)

        fieldnames = reader.fieldnames
        trackers_stats = [f.split('_') for f in fieldnames]
        trackers = set(ts[0] for ts in trackers_stats)
        stats_names = set(ts[1] for ts in trackers_stats)

        for tracker, stat_name in trackers_stats:
            if tracker not in data:
                data[tracker] = {}

            if stat_name not in data[tracker]:
                data[tracker][stat_name] = []

        for line in reader:
            for tracker in trackers:
                for stat_name in stats_names:
                    value = float(line['{}_{}'.format(tracker, stat_name)])
                    data[tracker][stat_name].append(value)

    if normalize:
        factors = {}
        for tracker in trackers:
            factors[tracker] = {}
            for stat_name in stats_names:
                factors[tracker][stat_name] = 1.0

        for tracker in trackers:
            for stat_name in stats_names:
                max_val = max([abs(d) for d in data[tracker][stat_name]])
                if max_val == 0:
                    max_val = 1
                factors[tracker][stat_name] = float(max_val)

        for tracker in trackers:
            for stat_name in stats_names:
                factor = factors[tracker][stat_name]
                d = data[tracker][stat_name]
                data[tracker][stat_name] = [val / factor for val in d]

    return data


def get_num_inputs(width: int,height: int) -> int:
    # dim of tiles
    framing_width = width
    framing_height = height
    number_inputs = framing_width * framing_height + framing_height
    return number_inputs


def get_num_trainable_parameters() -> int:
    num_inputs = get_num_inputs(t_width,t_height)
    hidden_layers = hidden_layer_tab
    num_outputs = 6  # U, D, L, R, A, B

    layers = [num_inputs] + hidden_layers + [num_outputs]
    num_params = 0
    for i in range(0, len(layers) - 1):
        L = layers[i]
        L_next = layers[i + 1]
        num_params += L * L_next + L_next

    return num_params


class MarioVisualInit(QtWidgets.QWidget):
    def __init__(self, parent, mario: EnvironementMario, size):
        super().__init__(parent)
        self.mario = mario
        self.size = size
        self.show()
