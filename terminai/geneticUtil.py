import numpy as np
import random
from typing import *

# INDIVIDUAL
class Individual(object):
    def __init__(self):
        pass


# POPULATION

class Population(object):
    def __init__(self, individuals: List[Individual]):
        self.individuals = individuals

    @property
    def num_individuals(self) -> int:
        return len(self.individuals)


    @property
    def num_genes(self) -> int:
        return self.individuals[0].chromosome.shape[1]



    @property
    def average_fitness(self) -> float:
        return (sum(individual.fitness for individual in self.individuals) / float(self.num_individuals))



    @property
    def fittest_individual(self) -> Individual:
        return max(self.individuals, key = lambda individual: individual.fitness)



    def calculate_fitness(self) -> None:
        for individual in self.individuals:
            individual.calculate_fitness()

    def get_fitness_std(self) -> float:
        return np.std(np.array([individual.fitness for individual in self.individuals]))



def mutation(gene: np.ndarray, proba: float) -> None:
    mutate_list = np.random.random(gene.shape) < proba
    mutation_ = np.random.normal(size=gene.shape)
    mutation_[mutate_list] *= 0.2
    gene[mutate_list] += mutation_[mutate_list]    # Update gene


def selection(population: Population, size_indiv_list: int) -> List[Individual]:
    individuals = sorted(population.individuals, key = lambda individual: individual.fitness, reverse=True)
    return individuals[:size_indiv_list]


def crossover_selection(population: Population, size_indiv_list: int) -> List[Individual]:
    selected_list = []
    for _ in range(size_indiv_list):
        select = np.random.choice(population.individuals, 5)
        best_indiv = max(select, key = lambda individual: individual.fitness)
        selected_list.append(best_indiv)

    return selected_list

#simulated binary cross from https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=7c07c3922755c57f9302433457b1b2baa515f318
def crossover(first_parent: np.ndarray, second_parent: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    randch =np.random.random(first_parent.shape)
    gamma = np.empty(first_parent.shape)

    gamma[randch <= 0.5] = (2 * randch[randch <= 0.5]) ** (1.0 / (101))
    gamma[randch > 0.5] = (1.0 / (2.0 * (1.0 - randch[randch > 0.5]))) ** (1.0 / (101))


    gene1 = 0.5 * ((1 + gamma)*first_parent + (1 - gamma)*second_parent)
    gene2 = 0.5 * ((1 - gamma)*first_parent + (1 + gamma)*second_parent)

    return gene1, gene2

