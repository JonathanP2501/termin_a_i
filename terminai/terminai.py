import retro
import shutil
import random
import sys
import math
import numpy as np
import argparse
import os

from typing import *
from PyQt5 import QtGui, QtWidgets
from PyQt5.QtGui import QPainter, QBrush, QPen, QPolygonF, QColor, QImage, QPixmap
from PyQt5.QtCore import Qt, QPointF, QTimer, QRect
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout, QLabel
from PIL import Image
from PIL.ImageQt import ImageQt

from utils import SuperMarioBros, EnemyRAMmap, StaticTileRAMmap, ColorMap, DynamicTileRAMmap
from environementMario import EnvironementMario, save, save_stats, get_num_trainable_parameters, get_num_inputs, MarioVisualInit
from geneticUtil import *

rand_mode=0

global_mutation_rate = 0.1

def parse_args():

    parser = argparse.ArgumentParser()
    # To specify population size
    parser.add_argument('--ch', dest='child_number', required=False, default=30, action='store_true')
    # To specify parents quantity
    parser.add_argument('--pa', dest='parent_number', required=False, default=10, action='store_true')
    # To desactivate interface
    parser.add_argument('--ni', dest='no_interface', required=False, default=False, action='store_true')

    args = parser.parse_args()
    return args

class GameWindow(QtWidgets.QWidget):
    def __init__(self, parent, size):
        super().__init__(parent)
        self.img_label = QtWidgets.QLabel(self)
        self.size = size
        self.window = None

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)

        width = int(self.window.shape[0] * 2.99)
        height = int(self.window.shape[1] * 1.98)

        gym_picture = QImage(self.window, self.window.shape[1], self.window.shape[0], QImage.Format_RGB888)
        picture = QImage(gym_picture)


        x = (self.window.shape[0] - width) // 2
        y = (self.window.shape[1] - height) // 2

        self.img_label.setGeometry(0, 0, width, height)

        pixel_maping = QPixmap(picture) # map picture
        pixel_maping = pixel_maping.scaled(width, height, Qt.KeepAspectRatio)
        self.img_label.setPixmap(pixel_maping)
        painter.end()

    def _update(self):
        self.update()

class InfoUI(QtWidgets.QWidget):
    def __init__(self, parent, size):
        super().__init__(parent)
        self.size = size
        self.grid = QtWidgets.QGridLayout()
        self.grid.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.grid)
        self._init_window()

    def uiBox(self, title: str, title_font: QtGui.QFont,
              content: str, content_font: QtGui.QFont) -> QHBoxLayout:
        title_label = QLabel()
        title_label.setFont(title_font)
        title_label.setText(title)
        title_label.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

        content_label = QLabel()
        content_label.setFont(content_font)
        content_label.setText(content)
        content_label.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

        hbox = QHBoxLayout()
        hbox.setContentsMargins(5, 0, 0, 0)
        hbox.addWidget(title_label, 1)
        hbox.addWidget(content_label, 1)
        return hbox

    def _init_window(self) -> None:
        info_vbox = QVBoxLayout()
        info_vbox.setContentsMargins(0, 0, 0, 0)
        ga_vbox = QVBoxLayout()
        ga_vbox.setContentsMargins(0, 0, 0, 0)

        generation_label = QLabel()
        generation_label.setText('Generation actuelle:')
        generation_label.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        self.generation = QLabel()
        self.generation.setText('1')
        self.generation.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        hbox_generation = QHBoxLayout()
        hbox_generation.setContentsMargins(100, 0, 0, 0)
        hbox_generation.addWidget(generation_label, 1)
        hbox_generation.addWidget(self.generation, 1)
        info_vbox.addLayout(hbox_generation)

        current_individual_label = QLabel()
        current_individual_label.setText('Individu actuel:')
        current_individual_label.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        self.current_individual = QLabel()
        self.current_individual.setText('1/{}'.format(args.parent_quantity))  # 10 parents
        self.current_individual.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        hbox_current_individual = QHBoxLayout()
        hbox_current_individual.setContentsMargins(100, 0, 0, 0)
        hbox_current_individual.addWidget(current_individual_label, 1)
        hbox_current_individual.addWidget(self.current_individual, 1)
        info_vbox.addLayout(hbox_current_individual)

        max_distance_label = QLabel()
        max_distance_label.setText('Progrès dans le niveau:')
        max_distance_label.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        self.max_distance = QLabel()
        self.max_distance.setText('0')
        self.max_distance.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        hbox_max_distance = QHBoxLayout()
        hbox_max_distance.setContentsMargins(100, 0, 0, 0)
        hbox_max_distance.addWidget(max_distance_label, 1)
        hbox_max_distance.addWidget(self.max_distance, 1)
        info_vbox.addLayout(hbox_max_distance)

        best_fitness_label = QLabel()
        best_fitness_label.setText('Meilleure valeure de fitness:')
        best_fitness_label.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        self.best_fitness = QLabel()
        self.best_fitness.setText('0')
        self.best_fitness.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        hbox_best_fitness = QHBoxLayout()
        hbox_best_fitness.setContentsMargins(100, 0, 0, 0)
        hbox_best_fitness.addWidget(best_fitness_label, 1)
        hbox_best_fitness.addWidget(self.best_fitness, 1)
        info_vbox.addLayout(hbox_best_fitness)

        self.grid.addLayout(info_vbox, 0, 0)
        self.grid.addLayout(ga_vbox, 0, 1)

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        global args
        self.pos_top = 150
        self.left = 150
        self.width = 1100
        self.height = 700

        self.name = 'T.E.R.minAI Mario'
        self.current_generation = 0

        self.new_gen = 0

        self._timer = QTimer(self)
        self._timer.timeout.connect(self.frame_update)

        self.keys = np.array( [0,0,0,0,0,0,0,0,0], np.int8) #key mapping  A B start select up down left right null to ind 8 0 4 5 6 7 8 2 3 1
        self.key_mapping_output = {
            0: 4,  # U
            1: 5,  # D
            2: 6,  # L
            3: 7,  # R
            4: 8,  # A
            5: 0   # B
        }

        indiv_list: List[Individual] = []
        indiv_number = 0
        parents_number = max(args.parent_quantity - indiv_number, 0)

        #generating the first population
        for _ in range(parents_number):
            indiv_number += 1
            indiv = EnvironementMario()
            indiv_list.append(indiv)

        self.best_fitness = 0.0
        self.current_indiv = 0
        self.pop = Population(indiv_list)

        self.mario = self.pop.individuals[self.current_indiv]
        
        self.distanceMax = 0
        self.max_fitness = 0.0

        #environement
        world = random.randint(1, 8);
        level = random.randint(1, 4);
        if not args.rand_mode:
            self.env = retro.make(game='SuperMarioBros-Nes', state=f'Level1-1')
        else:
            self.env = retro.make(game='SuperMarioBros-Nes', state=f'Level{world}-1')

        self._next_gen_size = args.child_quantity
        print("Individu: 1")
        if args.no_interface:
            self.env.reset()
        else:
            self.init_window()
            self.show()

        if args.no_interface:
            self._timer.start(1000 // 1000) #1000 frames per second if possible
        else:
            self._timer.start(1000 // 60) #frames per second QR LIMITATION

    def init_window(self) -> None:
        self.mainWidg = QtWidgets.QWidget(self)
        self.setCentralWidget(self.mainWidg)
        self.setWindowTitle(self.name)
        self.width //= 2.2

        self.setGeometry(self.pos_top, self.left, self.width, self.height)

        self.ui_window = InfoUI(self.mainWidg, (400, 600 - 380))
        self.ui_window.setGeometry(QRect(0, self.height - 220, self.width, 220))

        self.game_window = GameWindow(self.mainWidg, (514, 480))

        self.game_window.setGeometry(QRect(0, 0, self.width, self.height - 220))
        #self.setFixedSize(self.size)
        self.game_view = MarioVisualInit(self.mainWidg, self.mario, (1000 - 400, 600))
        if args.rand_mode:
            self.env.close()
            world = random.randint(1, 8);
            level = random.randint(1, 4);
            self.env = retro.make(game='SuperMarioBros-Nes', state=f'Level{world}-1')

        screen = self.env.reset()
        self.game_window.window = screen

    def next_gen(self) -> None:
        self.to_next_gen_str()
        self.current_indiv = 0
        print("Individu: "+str(self.current_indiv + 1))
        if not args.no_interface:
            self.ui_window.current_individual.setText('{}/{}'.format(self.current_indiv + 1, self._next_gen_size))

        best_ind = self.pop.fittest_individual
        result_folder = '/marioNN/population'
        best_ind_name = 'best_ind_gen{}'.format(self.current_generation - 1)
        save(result_folder, best_ind_name, best_ind)

        save_result_name = '/marioNN/stats/results.csv'
        save_stats(self.pop, save_result_name)

        self.pop.individuals = selection(self.pop, args.parent_quantity) #selection of individuals

        next_pop = []
        random.shuffle(self.pop.individuals)

        while len(next_pop) < self._next_gen_size: #while we need more indv for next pop
            first_parent, second_parent = crossover_selection(self.pop, 2) #selection of parents for crossover

            layers_len = len(first_parent.network.layer_nodes)
            first_child_parameters = {}
            second_child_parameters = {}

            #cross/mutation between parents
            for layer in range(1, layers_len):
                first_parent_weight_layer = first_parent.network.parameters['Weight' + str(layer)]
                first_parent_biais_layer = first_parent.network.parameters['Biais' + str(layer)]

                second_parent_weight_layer = second_parent.network.parameters['Weight' + str(layer)]
                second_parent_biais_layer = second_parent.network.parameters['Biais' + str(layer)]

                # Crossover
                first_child_weight_layer, second_child_weight_layer = crossover(first_parent_weight_layer, second_parent_weight_layer)
                first_child_biais_layer, second_child_biais_layer = crossover(first_parent_biais_layer, second_parent_biais_layer)

                # Mutate childs
                mutation(first_child_weight_layer, global_mutation_rate)
                mutation(second_child_weight_layer, global_mutation_rate)
                mutation(first_child_biais_layer, global_mutation_rate)
                mutation(second_child_biais_layer, global_mutation_rate)

                first_child_parameters['Weight' + str(layer)] = first_child_weight_layer
                second_child_parameters['Weight' + str(layer)] = second_child_weight_layer
                first_child_parameters['Biais' + str(layer)] = first_child_biais_layer
                second_child_parameters['Biais' + str(layer)] = second_child_biais_layer

                np.clip(first_child_parameters['Weight' + str(layer)], -1, 1, out=first_child_parameters['Weight' + str(layer)])
                np.clip(second_child_parameters['Weight' + str(layer)], -1, 1, out=second_child_parameters['Weight' + str(layer)])
                np.clip(first_child_parameters['Biais' + str(layer)], -1, 1, out=first_child_parameters['Biais' + str(layer)])
                np.clip(second_child_parameters['Biais' + str(layer)], -1, 1, out=second_child_parameters['Biais' + str(layer)])

            first_child = EnvironementMario(first_child_parameters, first_parent.hidden_layer_architecture,
                                            first_parent.hidden_layer_actv, first_parent.output_act,
                                            first_parent.lifespan)
            second_child = EnvironementMario(second_child_parameters, second_parent.hidden_layer_architecture,
                                             second_parent.hidden_layer_actv, second_parent.output_act,
                                             second_parent.lifespan)
            next_pop.extend([first_child, second_child]) #adding created childs to next gen

        # set next gen
        random.shuffle(next_pop)
        self.pop.individuals = next_pop


    def to_next_gen_str(self) -> None:
        self.current_generation += 1
        if not args.no_interface:
            txt = str(self.current_generation + 1)  # if we show the game
            self.ui_window.generation.setText(txt)
        print("New generation: " + str(self.current_generation + 1))

    def frame_update(self) -> None:
        ret = self.env.step(self.mario.button)

        if not args.no_interface:

            self.game_window.window = ret[0]
            self.ui_window.show()
            self.game_window._update()

        ram = self.env.get_ram() #get ingame ram memory (attention aux adresses!!!!)
        tiles = SuperMarioBros.get_all_tiles(ram)
        enemies = SuperMarioBros.get_enemy_position(ram)

        self.mario.update(ram, tiles, self.key_mapping_output)

        if self.mario.is_alive:
            if self.mario.max_dist_mario > self.distanceMax: #update max progress
                self.distanceMax = self.mario.max_dist_mario
                if not args.no_interface:
                    self.ui_window.max_distance.setText(str(self.distanceMax))
        else:
            self.mario.get_fitness_mario() #we update mario's fitness in this run
            fitness = self.mario.fitness
            
            if fitness > self.max_fitness: #update of max fitness
                self.max_fitness = fitness
                if(self.max_fitness > 1):
                    max_fitness = '{}'.format(self.max_fitness)
                else:
                    max_fitness = '{}'.format(0)


                if not args.no_interface:
                    self.ui_window.best_fitness.setText(max_fitness)
            # Next individual
            self.current_indiv += 1


            #current individual UI
            if (self.current_generation > self.new_gen and self.current_indiv == self._next_gen_size) or (self.current_generation == self.new_gen and self.current_indiv == args.parent_quantity):
                self.next_gen()
            else:
                if self.current_generation == self.new_gen:
                    current_pop = args.parent_quantity
                else:
                    current_pop = self._next_gen_size
                print("Individu: "+ str(self.current_indiv + 1))
                if not args.no_interface:
                    self.ui_window.current_individual.setText('{}/{}'.format(self.current_indiv + 1, current_pop))
            if args.rand_mode:
                self.env.close()
                world = random.randint(1, 8);
                level = random.randint(1, 4);
                self.env = retro.make(game='SuperMarioBros-Nes', state=f'Level{world}-1')
            if args.no_interface:
                self.env.reset()
            else:

                self.game_window.window = self.env.reset()
            
            self.mario = self.pop.individuals[self.current_indiv]

            if not args.no_interface:
                self.game_view.mario = self.mario
        

def parse_args():
    # Create an argument parser
    parser = argparse.ArgumentParser()

    # Add the argument for child quantity
    parser.add_argument('--ch', dest='child_quantity', type=int, required=False, default=30)

    # Add the argument for parent quantity
    parser.add_argument('--pa', dest='parent_quantity', type=int, required=False, default=10)

    # Add the argument to deactivate the interface
    parser.add_argument('--ni', dest='no_interface', required=False, default=False, action='store_true')

    # Add the argument to deactivate the interface
    parser.add_argument('--rnd', dest='rand_mode', required=False, default=False, action='store_true')

    # Parse the command-line arguments
    args = parser.parse_args()
    return args

if __name__ == "__main__":
    global args,child_quantity,parent_quantity
    args = parse_args()
    if os.path.exists('/marioNN'):
        shutil.rmtree('/marioNN')
    if args.child_quantity:
        child_quantity = args.child_quantity
    else:
        child_quantity = 30 #default
    if args.parent_quantity:
        parent_quantity = args.parent_quantity
    else:
        parent_quantity = 10 #default

    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())