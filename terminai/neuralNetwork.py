import numpy as np
from typing import *
import random


#to add nonlinearity and help the network learn complex patterns
Activate = NewType('Activate', Callable[[np.ndarray], np.ndarray])  #TO MODIFY
sigmoid = Activate(lambda X: 1.0 / (1.0 + np.exp(-X)))
tanh = Activate(lambda X: np.tanh(X))
relu = Activate(lambda X: np.maximum(0, X))
leaky_relu = Activate(lambda X: np.where(X > 0, X, X * 0.01))
linear = Activate(lambda X: X)

class NeuralNetwork(object):
    def __init__(self,
                 layer_nodes: List[int],
                 hidden_layer_actv: Activate,
                 output_activation: Activate,
                 random_seed: Optional[int] = None):

        self.inputs = None
        self.output = None

        self.parameters = {}
        self.layer_nodes = layer_nodes
        self.output_activation = output_activation
        self.hidden_layer_actv = hidden_layer_actv

        self.seed = random.Random(random_seed)

        for layer in range(1, len(self.layer_nodes)):
            #weights and biases init
            self.parameters['Weight' + str(layer)] = np.random.uniform(-1, 1, size=(self.layer_nodes[layer], self.layer_nodes[layer - 1]))
            self.parameters['Biais' + str(layer)] = np.random.uniform(-1, 1, size=(self.layer_nodes[layer], 1))
            self.parameters['A' + str(layer)] = None

    def nn_get_output(self, X: np.ndarray) -> np.ndarray:
        A_prev = X
        layer_len = len(self.layer_nodes) - 1

        for l in range(1, layer_len): #for each hidden layers
            weight = self.parameters['Weight' + str(l)]
            biais = self.parameters['Biais' + str(l)]
            Z = get_z_value(weight,biais, A_prev)

            A_prev = self.hidden_layer_actv(Z)
            self.parameters['A' + str(l)] = A_prev

        weight = self.parameters['Weight' + str(layer_len)]
        biais = self.parameters['Biais' + str(layer_len)]

        Z = get_z_value(weight,biais, A_prev)
        output = self.output_activation(Z)
        self.parameters['A' + str(layer_len)] = output

        self.output = output
        return output

    def softmax(self, X: np.ndarray) -> np.ndarray:
        return np.exp(X) / np.sum(np.exp(X), axis=0)

def get_z_value(weight: str,biais: str, prev: np.ndarray) -> np.ndarray:
    return np.dot(weight, prev) + biais

def get_activation(name: str) -> Activate: #TO MODIFY
    actv_func = [('relu', relu),
                   ('sigmoid', sigmoid),
                   ('linear', linear),
                   ('leaky_relu', leaky_relu),
                   ('tanh', tanh),
                   ]

    func = [active[1] for active in actv_func  if active[0].lower() == name.lower()]
    assert len(func) == 1

    return func[0]
