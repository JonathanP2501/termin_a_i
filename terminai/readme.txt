Utilisation et installation:

Librairies:
	pyqt5 (pour l'interface et l'affichage)
	numpy (librairie commune pour des fonctions mathématiques et random)
	gym
	gym-retro

Ce programe utilisant la librairie GymRetro d'openAI il est au préalable necessaire d'installer cette dernière et d'importer la rom du jeu dessus.

Importer la rom du jeu:

	placez la rom dans le repertoire de votre choix puis utilisez la commande

	python -m retro.import "/chemin_vers_rom/super mario bros. (world)"

	le retour console sera:
	Importing SuperMarioBros-Nes
	Imported 1 games

Utilisation du programme:
	Lancement: python terminai.py
	options:
		-Pour la version avec niveau randomisé, False par defaut: --rnd
		-Pour avoir une population (enfants produits a chaque génération) de 50 par exemple, 30 par defaut: --ch 50
		-Pour avoir une population de départ (parents) de 15 , 10 par defaut: --nd 15
		-Pour désactiver l'affichage , False par defaut: --ni

Resultats:
	les resultats sont stockés dans D:\marioNN si le programme est par exemple lancé sur le disque D:
