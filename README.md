Termin_A_I est un projet dans le cadre des travaux d'études et de recherche du groupe de projet T.E.R.minator de M1 informatique de Montpellier.
Les objectifs de ce projet sont:
-Le devellopement d'une I.A capable de jouer à des jeux vidéos (notament des plateformer 2D) basé sur un algorythme génétique.
-La comparaison des performances d'une I.A génétique avec une I.A par reforcement utilisant un reseau de neuronnes dans le cadre de jeux situé dans un cadre fixe ou procédural.
Lien du rapport Overleaf: https://www.overleaf.com/7856148672xwqrzbsnqhvm

Utilisation et installation:

Librairies:
	pyqt5 (pour l'interface et l'affichage)
	numpy (librairie commune pour des fonctions mathématiques et random)
	gym (emulateur d'OpenAI)
	gym-retro

Ce programe utilisant la librairie GymRetro d'openAI il est au préalable necessaire d'installer cette dernière et d'importer la rom du jeu dessus.

Importer la rom du jeu:

	placez la rom dans le repertoire de votre choix puis utilisez la commande

	python -m retro.import "/chemin_vers_rom/super mario bros. (world)"

	le retour console sera:
        Importing SuperMarioBros-Nes
        Imported 1 games

Utilisation du programme:
	Lancement: python terminai.py
	options:
		-Pour la version avec niveau randomisé, False par defaut: --rnd
		-Pour avoir une population (enfants produits a chaque génération) de 50 par exemple, 30 par defaut: --ch 50
		-Pour avoir une population de départ (parents) de 15 , 10 par defaut: --nd 15
		-Pour désactiver l'affichage , False par defaut: --ni
		-Pour choisir 100 générations, 10 000 par defaut puisque les données sont sauvegardées a chaque fin de generation: --gen 100

